using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System.IO;
using System.Collections.Generic;

namespace UnitTestProject2
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {           
            var dc = new DesiredCapabilities();
            dc.SetCapability("app", @"C:/windows/system32/calc.exe");
            var driver = new RemoteWebDriver(new Uri("http://localhost:9999"), dc);
            Thread.Sleep(5000);
            var window = driver.FindElementByClassName("CalcFrame");
            var viewMenuItem = window.FindElement(By.Id("MenuBar")).FindElement(By.Name("View"));

            viewMenuItem.Click();
            viewMenuItem.FindElement(By.Name("Scientific")).Click();

            viewMenuItem.Click();
            viewMenuItem.FindElement(By.Name("History")).Click();

            window.FindElement(By.Id("132")).Click(); // 2
            window.FindElement(By.Id("93")).Click(); // +
            window.FindElement(By.Id("134")).Click(); // 4
            window.FindElement(By.Id("97")).Click(); // ^
            window.FindElement(By.Id("138")).Click(); // 8
            window.FindElement(By.Id("121")).Click(); // =

            driver.Close();
        }
    }
}
